#!/bin/bash

echo Enter a number to reverse: 
read NUMBER
num=0
while [ $NUMBER -gt 0 ]
do
num=`expr $num \* 10`
t=`expr $NUMBER % 10`
num=`expr $num + $t`
NUMBER=`expr $NUMBER / 10`
done
echo  Reversed Number:  $num
