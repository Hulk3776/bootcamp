#!/bin/bash

ReverseNumber() {
echo "Enter a number to reverse: "
read N
length=`expr length "$N"`
if [ $length -eq "0" ]
then  
echo "Execute The Script Correctly"
else
sd=0
rev=0
while [ $N -gt 0 ]
do
sd=$(( $N % 10 ))
rev=`expr $rev \* 10 + $sd`
N=$(( $N / 10 ))
done
echo "Reversed Number: $rev"
fi
}

ReverseNumber  
