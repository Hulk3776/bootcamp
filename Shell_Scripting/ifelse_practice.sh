#!/bin/bash

echo "Enter a num: "
read NUM

if [[ $NUM -lt 0 ]]
then 
echo "Negative number"
elif (( $NUM % 2 == 0 ))
then 
echo "Even Number"
else  
echo "Odd Number"
fi
