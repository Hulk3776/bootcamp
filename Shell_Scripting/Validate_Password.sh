#!/bin/bash

echo "Enter a Password: "
read PASSWORD
ValidatePassword() {
len="${#PASSWORD}"

if test $len -ge 8 ; then
echo "$PASSWORD" | grep -q [0-9]
if test $? -eq 0 ; then
echo "$PASSWORD" | grep -q [A-Z]
if test $? -eq 0 ; then
echo "$PASSWORD" | grep -q [a-z]
if test $? -eq 0 ; then
echo "Strong Password"
else
echo "Weak Password -> Should include a lower case letter"x
fi
else
echo "Weak Password -> Should include upper case letter"
fi
else
echo "Weak Password -> Should include Number"
fi
else
echo "Weak Password -> Password length should have at least 8 characters"
fi 
}
ValidatePassword
