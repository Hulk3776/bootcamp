#!/bin/bash

# Defining a function
SayHello(){
echo "$0"
echo "Hello $1"
echo "Hello to you to $2"
echo $3
return 100
}

#call the function
SayHello Aryaman Aditi Shivendu

#Capture the value returned from the previous line
returned=$?

echo "Returned value: $returned"
